from flask import jsonify
from data.datamodel import app, session
from data.starsystem import StarSystem
from data.utils import param


@app.route('/api/v1/systems-near/byname/<string:name>', methods=["GET"])
def near_v1_byname(name):
    """
    Get a list of starsystems surrounding the given system.
    ---
    parameters:
      - name: name
        in: path
        schema:
          type: string
          example: Sol
        required: true
        description: Name of star system to start from.
      - name: dist
        in: query
        schema:
          type: integer
          example: 5
          maximum: 45
          default: 25
        required: false
        example: 5
        description: Max light years from the named star system
    responses:
      "200":
        description: A list of nearby systems
        schema:
          type: array
          items:
            $ref: "#/definitions/StarSystem"
    """
    resp = []
    radius = param("dist", 25)
    with session() as s:
        star = s.query(StarSystem).filter(StarSystem.name == name).first()
        if star:
            near = star.radius(s, radius)
            for item in near:
                resp.append(item.to_dict())

    return jsonify(resp)


@app.route('/api/v1/systems-near/byid/<int:id>', methods=["GET"])
def near_v1_byid(id):
    """
    Get all the system ids, names and distances nearby
    ---
    parameters:
      - name: id
        in: path
        schema:
          type: integer
          example: 1
        required: true
        description: ID of star system to start from.
      - name: dist
        in: query
        schema:
          type: integer
          example: 5
          maximum: 45
          default: 25
        required: false
        example: 5
        description: Max light years from the starting star system
    responses:
      "200":
        description: A list of nearby systems
        schema:
          type: array
          items:
            $ref: "#/definitions/StarSystem"
    """
    resp = []
    radius = param("dist", 25)
    with session() as s:
        star = s.query(StarSystem).filter(StarSystem.id == id).first()
        if star:
            near = star.radius(s, radius)
            for item in near:
                resp.append(item.to_dict())

    return jsonify(resp)


@app.route('/api/v1/system/byname/<string:name>', methods=["GET"])
def system_v1_byname(name):
    """
    Get a system
    ---
    parameters:
      - name: name
        in: path
        schema:
          type: string
          example: Sol
        required: true
        description: Name of star system to search for.

    definitions:
      StarSystem:
        type: object
        properties:
          id:
            type: integer
          name:
            type: string
          economy:
            type: string
          population:
            type: integer
          x:
            type: double
          y:
            type: double
          z:
            type: double
          updated:
            type: integer
            description: unix timestamp of last update
        example:
          id: 17072
          name: Sol
          economy: Refinery
          population: 22780871769
          updated: 1526181262
          x: 0
          y: 0
          z: 0

    responses:
      "200":
        description: A star system
        schema:
          $ref: "#/definitions/StarSystem"

    """
    with session() as s:
        star = s.query(StarSystem).filter(StarSystem.name == name)
        if star.count():
            return star.first().to_json()
    return jsonify({})


@app.route('/api/v1/system/byid/<int:id>', methods=["GET"])
def system_v1_byid(id):
    """
    Get a system
    --
    parameters:
      - name: id
        in: path
        schema:
          type: integer
          example: 17072
        required: true
        description: ID of star system to search for.
    responses:
      "200":
        description: A star system
        schema:
          $ref: "#/definitions/StarSystem"
    """
    with session() as s:
        star = s.query(StarSystem).filter(StarSystem.id == id)
        if star.count():
            return star.first().to_json()
    return jsonify({})


def register():
    pass

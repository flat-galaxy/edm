"""
Update data from EDDN as we go
"""
from sqlalchemy import and_

from data.utils import eprint
from .datamodel import session
from .starsystem import StarSystem, Station
from .prices import CommodityInfo, CommodityPrice


SCHEMA_REF = "$schemaRef"
COMMODITY_SCHEMA = 'https://eddn.edcd.io/schemas/commodity/3'


def get_station_by_names(s, starname, stationname):
    """
    Get the correct station in a system (there are stations and stars with identical names, but not together)
    :parma s: session
    :param starname: name of the star system
    :param stationname: name of the station
    :return:
    """
    stars = s.query(StarSystem.id).filter(StarSystem.name == starname)
    if stars and stars.count():
        for starid in stars.all():
            starid = starid[0]
            # find the station
            stations = s.query(Station.id).filter(
                and_(
                    Station.system_id == starid,
                    Station.name == stationname
                ))
            if stations and stations.count() == 1:
                station = stations.first()[0]
                return s.query(Station).get(station)

    # no match
    return None


def get_price(s, stationid, commodityname):
    """
    Get the price entry for a commodity at a particular station
    :param s:
    :param stationid:
    :param commodityname:
    :return:
    """
    cq = s.query(CommodityInfo.id).filter(CommodityInfo.name == commodityname)
    if cq and cq.count():
        cid = cq.first()[0]

        # get the actual price object for this
        pq = s.query(CommodityPrice).filter(
            and_(
                CommodityPrice.station_id == stationid,
                CommodityPrice.commodity_id == cid
            ))
        if pq and pq.count():
            return pq.first()

    return None


def handle_eddn_message(data):
    """
    Process an update from EDDN
    :param data:
    :return:
    """
    if data:
        if data.get(SCHEMA_REF, "") == COMMODITY_SCHEMA:
            # update prices!

            starname = data["message"]["systemName"]
            statname = data["message"]["stationName"]
            with session() as s:
                station = get_station_by_names(s, starname, statname)
                if station:
                    eprint("update prices at {} - {}".format(statname, starname))
                    # go through all the commodities and update prices
                    for item in data["message"]["commodities"]:
                        price = get_price(s, station.id, item["name"])
                        if price:
                            price.from_eddn_dict(data["message"]["timestamp"], item)

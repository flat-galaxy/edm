"""
Store and maintain price data
"""
import time
import dateutil.parser as dateparser
import simplejson
import requests

from data.utils import eprint
from .datamodel import session, db, EDMModelJson, ensure_db


class CommodityInfo(db.Model, EDMModelJson):
    """
    A single commodity name
    """
    id = db.Column(db.Integer, primary_key=True)
    ed_id = db.Column(db.Integer, index=True, unique=True)  # the Elite:Dangerous id for this item
    name = db.Column(db.Text, index=True)
    category_id = db.Column(db.Integer, index=True)
    category = db.Column(db.Text, index=True)
    average_price = db.Column(db.Integer)
    rare = db.Column(db.Boolean, index=True)


class CommodityPrice(db.Model, EDMModelJson):
    """
    Buy/sell price, supply, demand at a station
    """
    id = db.Column(db.Integer, primary_key=True)
    commodity_id = db.Column(db.Integer, index=True)
    station_id = db.Column(db.Integer, index=True)
    stock = db.Column(db.Integer)
    demand = db.Column(db.Integer)
    buy = db.Column(db.Integer)
    sell = db.Column(db.Integer)
    collected_at = db.Column(db.Integer, index=True)

    def from_listings_dict(self, data):
        """
        Given a row from listings.csv, populate ourself
        :param data:
        :return:
        """
        self.id = data.get("id")
        self.commodity_id = data.get("commodity_id")
        self.station_id = data.get("station_id")
        self.demand = data["demand"]
        self.stock = data["supply"]
        self.buy = data["buy_price"]
        self.sell = data["sell_price"]
        self.collected_at = data["collected_at"]

    def from_eddn_dict(self, timestamp, data):
        """
        Given an eddn commodity entry populate ourself if the eddn data is newer
        :param timestamp: a time string ( YYYY-MM-DDTHH:mm:ssZ )
        :param data:
        :return:
        """
        msgtime = dateparser.parse(timestamp)
        unixtime = time.mktime(msgtime.timetuple())

        if unixtime > self.collected_at:
            self.demand = data.get("demand")
            self.buy = data.get("buyPrice")
            self.sell = data.get("sellPrice")
            self.stock = data.get("stock")
            self.collected_at = unixtime

    @staticmethod
    def set_newer_price(s, data):
        last_updated = s.query(CommodityPrice.collected_at).filter(
            CommodityPrice.id == data["id"]
        ).scalar()
        if last_updated is None:
            price = CommodityPrice()
            price.from_listings_dict(data)
            s.bulk_save_objects([price])
        elif last_updated < data["collected_at"]:
            CommodityPrice.update_existing_price(s, data)

    @staticmethod
    def add_new_price(s, data):
        price = CommodityPrice()
        price.from_listings_dict(data)
        s.bulk_save_objects([price])

    @staticmethod
    def update_existing_price(s, data):
        price = s.query(CommodityPrice).get(data["id"])
        if price:
            price.from_listings_dict(data)


def eprint_progress_length(collection, msg):
    if len(collection) % 1000 == 0:
        eprint(msg)


def process_listings_csv():
    """
    Process the listings.csv from eddb
    :return:
    """
    # find out what the last update time was for things we have in the csv
    last_updates = dict()

    with session() as s:
        # get the "collected_at" value for every price so we can avoid unnecessary updates
        last_updated_q = s.query(CommodityPrice.id, CommodityPrice.collected_at)
        total = last_updated_q.count()
        for pid, collected in last_updated_q.values(CommodityPrice.id, CommodityPrice.collected_at):
            last_updates[pid] = collected
            eprint_progress_length(
                last_updates,
                "finding last collected times {}/{}".format(len(last_updates), total))

    eprint("Downloading prices..")
    columns = []
    resp = requests.get("https://eddb.io/archive/v5/listings.csv")
    eprint("Importing prices..")
    lines = resp.content.splitlines()
    data = {}
    # turn all the lines into a dict and discard anything older than 48hrs

    now = time.time()
    earliest = now - (2 * 24 * 60 * 60)  # 2 days

    for line in lines:
        if line:
            text = line.decode("unicode-escape")
            values = None
            if not columns:
                columns = text.split(",")
            else:
                values = []
                for value in text.split(","):
                    if value.isdigit():
                        values.append(int(value))
                    else:
                        values.append(0)

            if values and columns:
                price = dict(zip(columns, values))

                if price["collected_at"] > earliest:
                    data[price["id"]] = price
                    eprint_progress_length(
                        data,
                        "make price dict {}/{}".format(len(data), len(lines) - 1))

    # save some memory
    lines.clear()

    # now only update records that are newer
    saved = 0
    newprices = 0
    updatedprices = 0
    with session() as s:
        for pid in data:
            if pid not in last_updates:
                # new entry
                CommodityPrice.add_new_price(s, data[pid])
                newprices += 1
            elif data[pid]["collected_at"] > last_updates[pid]:
                # updated entry
                CommodityPrice.update_existing_price(s, data[pid])
                updatedprices += 1
            saved += 1
            if saved % 1000 == 0:
                eprint("saved {}/{}  new={} updated={}".format(
                    saved, len(data), newprices, updatedprices))


def refresh_commodity_info():
    """
    Sync the current galaxy averages and names from eddb
    :return:
    """
    raw = requests.get("https://eddb.io/archive/v5/commodities.json")
    data = simplejson.loads(raw.content)
    ensure_db()

    with session() as s:
        for item in data:
            found = s.query(CommodityInfo).filter(CommodityInfo.id == item["id"])

            if found.count():
                eprint("add commodity {}".format(item["name"]))
                info = found.first()
            else:
                eprint("update commodity {}".format(item["name"]))
                info = CommodityInfo()
                s.add(info)
                info.id = item["id"]
                info.ed_id = item["ed_id"]
                info.name = item["name"]
                info.rare = item["is_rare"] == 1

            info.average_price = item["average_price"]
            info.category_id = item["category_id"]
            info.category = item["category"]["name"]


if __name__ == "__main__":
    refresh_commodity_info()
    process_listings_csv()

from data.utils import eprint
from .datamodel import db, EDMModelJson, session
import sqlalchemy.sql as sql


class StarSystem(db.Model, EDMModelJson):
    """
    A simple model of a star system
    """
    __tablename__ = "starsystem"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, index=True)
    updated = db.Column(db.Integer, index=True)  # unixtime
    x = db.Column(db.Float)
    y = db.Column(db.Float)
    z = db.Column(db.Float)

    # index the integer component of coordinates to make location based searches easier
    intx = db.Column(db.Float, index=True)
    inty = db.Column(db.Float, index=True)
    intz = db.Column(db.Float, index=True)

    stations = db.relationship("Station", back_populates="system")
    population = db.Column(db.Integer)
    economy = db.Column(db.Text, index=True)

    def to_dict(self):
        data = super(StarSystem, self).to_dict()
        data["population"] = self.population
        data["economy"] = self.economy
        data["x"] = self.x
        data["y"] = self.y
        data["z"] = self.z
        data["updated"] = self.updated
        return data

    def radius(self, s, dist):
        """
        Return all the star systems within a cube around this
        :param s: session
        :param dist: approx max integer light years away
        :return:
        """
        dist = min(dist, 50)
        nearby = s.query(StarSystem).filter(
            sql.and_(
                StarSystem.intx >= self.intx - dist,
                StarSystem.intx <= self.intx + dist,
                StarSystem.inty >= self.inty - dist,
                StarSystem.inty <= self.inty + dist,
                StarSystem.intz >= self.intz - dist,
                StarSystem.intz <= self.intz + dist,
            )
        )
        if nearby:
            return nearby.all()
        return []


    @staticmethod
    def update(s, data):
        """
        Add or update a system
        :param s:
        :param data:
        :return:
        """
        assert data
        sysid = data.get("id")
        star = s.query(StarSystem).get(sysid)

        population = data.get("population")
        name = data.get("name")

        if not star:
            # make a new one
            newsys = StarSystem()
            newsys.id = sysid
            newsys.name = name
            eprint("add new {}".format(name))
            newsys.population = population
            newsys.x = data.get("x")
            newsys.y = data.get("y")
            newsys.z = data.get("z")
            newsys.intx = int(newsys.x)
            newsys.inty = int(newsys.y)
            newsys.intz = int(newsys.z)
            newsys.economy = data.get("primary_economy", "unknown")
            newsys.updated = data.get("updated_at")
            s.add(newsys)
        else:
            # update existing
            eprint("update {}".format(name))
            changed = star.updated < data.get("updated_at")

            if changed:
                star.population = population
                star.updated = data.get("updated_at")


class Station(db.Model, EDMModelJson):
    """
    A simple model of a station
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, index=True)
    updated = db.Column(db.Integer, index=True)  # unixtime
    system_id = db.Column(db.Integer, db.ForeignKey("starsystem.id"))
    system = db.relationship("StarSystem", back_populates="stations")
    pad = db.Column(db.Text, index=True)

    @staticmethod
    def update_station(s, data):
        """
        Update a station
        :param s:
        :param data:
        :return:
        """
        assert data
        statid = data.get("id")
        name = data.get("name")
        sysid = data.get("system_id")
        updated = data.get("updated_at")

        station = s.query(Station).get(statid)
        if station:
            eprint("update station {}".format(name))
        else:
            # new station
            eprint("add station {}".format(name))
            station = Station()
            station.id = statid
            station.name = name
            station.pad = data.get("max_landing_pad_size")
            station.system_id = sysid
            s.add(station)

        if not station.updated or updated > station.updated:
            station.updated = updated

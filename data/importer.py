"""
Handle new data as it arrives or is read from eddb/*.jsonl files
"""
import os
import sys
import simplejson
from .utils import eprint

from .datamodel import db, DBFILE, session
from .starsystem import StarSystem, Station


def process_systems_jsonl(filename):
    """
    Read a jsonl file from eddb containing system details
    :param filename:
    :return:
    """
    eprint("Importing systems from file: ")

    with open(filename, "r") as jsonfile:
        while True:
            line = jsonfile.readline()
            if not line:
                break
            with session() as sess:
                data = simplejson.loads(line)
                if data:
                    update_system(sess, data)


def process_stations_jsonl(filename):
    """
    Read a jsonl file from eddb containing station details
    :param filename:
    :return:
    """
    eprint("Importing systems from file: ")

    with open(filename, "r") as jsonfile:
        while True:
            line = jsonfile.readline()
            if not line:
                break
            with session() as sess:
                data = simplejson.loads(line)
                if data:
                    update_station(sess, data)


def update_station(s, data):
    """
    Update a station
    :param s:
    :param data:
    :return:
    """
    Station.update_station(s, data)


def update_system(s, data):
    """
    Add/Update a single system
    :param s: a session
    :param data:
    :return:
    """
    StarSystem.update(s, data)


if __name__ == "__main__":
    if not os.path.exists(DBFILE):
        db.create_all()
    if len(sys.argv) == 2:
        filename = sys.argv[1]
        if os.path.basename(filename).startswith("systems"):
            process_systems_jsonl(filename)
        if os.path.basename(filename).startswith("stations"):
            process_stations_jsonl(filename)
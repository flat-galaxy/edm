import sys
from flask import request


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def param(name, defaultvalue=0):
    value = request.args.get(name, defaultvalue)
    if isinstance(defaultvalue, int):
        if isinstance(value, str):
            if value.isdigit():
                value = int(value)
    return value

import os
from contextlib import contextmanager

from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy

DATADIR = os.path.dirname(os.path.abspath(__file__))
DBFILE = os.path.join(DATADIR, "db", "edm.sqlite")

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DBFILE

db = SQLAlchemy(app)


def ensure_db():
    """
    Make sure the database exists
    :return:
    """
    db.create_all()


@contextmanager
def session():
    """Provide a transactional scope around a series of operations."""
    sess = db.create_scoped_session()
    try:
        yield sess
        sess.commit()
    except:
        sess.rollback()
        raise
    finally:
        sess.close()


class EDMModelJson(object):
    """
    Base class mixin for models
    """

    def to_dict(self):
        """
        Make a dict representation
        :return:
        """
        data = {}
        if hasattr(self, "id"):
            data["id"] = self.id

        if hasattr(self, "name"):
            data["name"] = self.name

        return data

    def to_json(self):
        """
        Make a json representation
        :return:
        """
        return jsonify(self.to_dict())

FROM python:3.5

RUN pip install virtualenv

RUN mkdir -p /edm/data/eddb && mkdir -p /edm/data/db && chmod -R 777 /edm/data/eddb

ADD *.py /edm/
ADD data/*.py /edm/data/
ADD *.sh /edm/
ADD requirements.txt /edm/

WORKDIR /edm

RUN bash setup.sh

EXPOSE 5000

VOLUME /edm/data/db

CMD ["start.sh"]
ENTRYPOINT ["/bin/bash"]

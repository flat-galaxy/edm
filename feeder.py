#!/usr/bin/python
import sys
import subprocess
from eddn import Listener
from data.updater import handle_eddn_message

feed = Listener()
feed.subscribe(handle_eddn_message)
feed.start()

rv = subprocess.call(sys.argv[1:], shell=False)
sys.exit(rv.returncode)

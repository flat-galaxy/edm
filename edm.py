import v1buy
import v1system
from data.datamodel import app
from flasgger import Swagger


v1system.register()
v1buy.register()


app.config['SWAGGER'] = {
    'title': 'ED:Merchant REST API',
}
swagger = Swagger(app)

application = app

if __name__ == '__main__':
    app.run()


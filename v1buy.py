"""
REST methods to find the nearest places to buy a cargo
"""
from flask import jsonify
from sqlalchemy import and_

from data.datamodel import app, session
from data.starsystem import StarSystem
from data.prices import CommodityInfo, CommodityPrice
from data.utils import param


@app.route('/api/v1/buy-near/byname/<string:sysname>/<string:commodity>', methods=["GET"])
def buy_v1_near(sysname, commodity):
    """
    Find the stations near a system that sell a specific commodity
    ---
    parameters:
      - name: sysname
        in: path
        schema:
          type: string
          example: Lave
        required: true
        description: Name of star system to start from.
      - name: commodity
        in: path
        schema:
          type: string
          example: Gold
        required: true
      - name: dist
        in: query
        schema:
          type: integer
          example: 5
          maximum: 45
          default: 25
        required: false
        example: 5
        description: Max light years from the starting star system
      - name: pad
        in: query
        schema:
          type: string
          enum: [M, L]
        description: Suitable for ship size (medium or large)
      - name: stock
        int query:
        schema:
          type: integer
        description: Minimum available stock required

    definitions:
      CommodityStockPrice:
        type: object
        properties:
          buy:
            type: integer
          pad:
            type: string
          station:
            type: string
          stock:
            type: integer
          system:
            type: string
      BuyNearResponse:
        type: object
        properties:
          reference_system:
            type: string
          commodity_search:
            type: string
          minimum_pad:
            type: string
            enum: [M, L]
          max_radius:
            type: double
          min_stock:
            type: integer
          sellers:
            type: array
            items:
              $ref: "/#definitions/CommodityStockPrice"

    responses:
      "200":
        description: Details of the search and matching sellers
        schema:
          $ref: "#/definitions/BuyNearResponse"

    """
    radius = param("dist", 25)
    padsize = param("pad", "L")
    minstock = param("stock", 1)

    def can_dock(station):
        # return True if the requested pad is at the given station
        if station.pad == "L":
            return True  # everyone can dock at large pads
        if padsize == "L":  # can only dock at large pads
            return padsize == station.pad
        # any pad
        return True

    sellers = []
    with session() as s:
        cargo = s.query(CommodityInfo).filter(CommodityInfo.name == commodity)
        if cargo and cargo.count():
            cargo = cargo.first()
            star = s.query(StarSystem).filter(StarSystem.name == sysname).first()
            if star:
                nearby = star.radius(s, radius)
                # get all the stations that sell the given commodity

                for near in nearby:
                    for station in near.stations:
                        # include large pads if requested
                        if can_dock(station):
                            # get the price data for this cargo at this station
                            price = s.query(CommodityPrice).filter(
                                and_(
                                    CommodityPrice.commodity_id == cargo.id,
                                    CommodityPrice.station_id == station.id
                                )).first()
                            if price:
                                if price.stock > minstock:
                                     sellers.append({
                                         "system": near.name,
                                         "station": station.name,
                                         "pad": station.pad,
                                         "stock": price.stock,
                                         "buy": price.buy
                                     })

    return jsonify({
        "reference_system": sysname,
        "commodity_search": commodity,
        "minimum_pad": padsize,
        "max_radius": radius,
        "min_stock": minstock,
        "sellers": sellers
    })


def register():
    pass

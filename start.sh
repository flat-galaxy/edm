#!/bin/bash

set -ex
pwd

source ./venv/bin/activate

wget -O data/eddb/systems_populated.jsonl https://eddb.io/archive/v5/systems_populated.jsonl
python -m data.importer data/eddb/systems_populated.jsonl

wget -O data/eddb/stations.jsonl https://eddb.io/archive/v5/stations.jsonl
python -m data.importer data/eddb/stations.jsonl

python -m data.prices

python feeder.py gunicorn -w 2 --bind 0.0.0.0:5000 edm

import json
import threading
import os
import traceback
import simplejson
import zlib
import zmq
import sys
import time


RELAY = 'tcp://eddn.edcd.io:9500'
TIMEOUT = 600000


class Listener(object):
    """
    Listen for traffic from EDDN
    """

    def __init__(self):
        self.recv = []
        self.exit = False
        self.thread = None
        self.recent = []
        self.last_dump = 0

    def start(self):
        """
        Start the relay loop
        :return:
        """
        self.thread = threading.Thread(target=self.loop)
        self.thread.setDaemon(True)
        self.thread.start()

    def stop(self):
        """
        Stop the relay loop
        :return:
        """
        self.exit = True
        if self.thread:
            self.thread.join()

    def subscribe(self, handler):
        """
        Add an event handler
        :param handler:
        :return:
        """
        self.recv.append(handler)

    def respond(self, content):
        """
        Send the message to the subscribers
        :param content:
        :return:
        """
        for handler in self.recv:
            try:
                handler(content)
            except Exception as err:
                print("Handler error {}".format(type(err)))
                if hasattr(err, "message"):
                    print(" - {}".format(err.message))
                traceback.print_exc()

    def loop(self):
        context = zmq.Context()
        subscriber = context.socket(zmq.SUB)

        subscriber.setsockopt(zmq.SUBSCRIBE, b"")
        subscriber.setsockopt(zmq.RCVTIMEO, TIMEOUT)

        while not self.exit:
            try:
                subscriber.connect(RELAY)

                while not self.exit:
                    message = subscriber.recv()

                    if not message:
                        time.sleep(0.1)
                        break

                    data = zlib.decompress(message)
                    content = simplejson.loads(data)
                    if content:
                        self.respond(content)

            except zmq.ZMQError as err:
                print("ZMQ error {} {}".format(type(err), str(err)))
                sys.stdout.flush()

            finally:
                subscriber.disconnect(RELAY)
                time.sleep(3)


if __name__ == '__main__':
    Listener().loop()

#!/bin/bash

set -ex

virtualenv --clear -p $(which python3.5) ./venv

source ./venv/bin/activate

pip install -r requirements.txt

